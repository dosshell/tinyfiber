/*
MIT License

Copyright (c) 2020 Markus Lindelöw

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef TINYFIBER_H
#define TINYFIBER_H

#include <stdint.h>
#include <stddef.h>
#include <atomic>
#ifndef _WIN32
#include <mutex>
#include <optional>
#include <ucontext.h>
#endif
#ifdef __cplusplus
extern "C"
{
#endif
    typedef struct TfbContext TfbContext;

    // Internal structure, init to zero to use. Writes will result in UF
#ifdef _WIN32
    typedef struct
    {
        void* _fiber;
        std::atomic_int64_t _counter;
        void* _lock;
    } TfbWaitHandle;
#else
    typedef struct
    {
        ucontext_t* _fiber;
        std::atomic_int64_t _counter;
        std::mutex _lock;
    } TfbWaitHandle;
#endif
    typedef struct
    {
        void (*func)(void*);
        void* user_data;
        TfbWaitHandle* wait_handle;
    } TfbJobDeclaration;

    const int TFB_ALL_CORES = 0;

    /**
     * @brief Creates a new fiber system context.
     *
     * @code
     * TfbContext* fiber_system = NULL;
     * tfb_init_ext(&fiber_system, TFB_ALL_CORES);
     * tfb_free_ext&fiber_system);
     * @endcode
     *
     * @param fiber_system is your in-out pointer to a TfbContext pointer. Initialize to NULL before use.
     * @param max_threads descibes how many working threads your fiber system should have.
     * @return 0 if successful, otherwise the error code.
     * @see tfb_init()
     * @see rfb_free_ext()
     */
    int tfb_init_ext(int max_threads);

    inline int tfb_init()
    {
        return tfb_init_ext(TFB_ALL_CORES);
    }

    int tfb_free();

    int tfb_add_jobdecl(TfbJobDeclaration* job_declaration);

    int tfb_add_jobdecls(TfbJobDeclaration jobs[], int64_t elements);

    inline int tfb_add_job(void (*func)(void*), void* user_data, TfbWaitHandle* wh)
    {
        TfbJobDeclaration job = {func, user_data, wh};
        return tfb_add_jobdecl(&job);
    }

    int tfb_await(TfbWaitHandle* wait_handle);

    int64_t tfb_thread_id();

#ifdef __cplusplus
}
#endif

#endif
