choco feature enable -n allowGlobalConfirmation
choco install cmake
call RefreshEnv.cmd
set PATH=%PATH%;C:\Program Files\CMake\bin\
mkdir build64
cd build64
cmake.exe -G "Visual Studio 16 2019" -A "x64" ../
cmake.exe --build . --target ALL_BUILD --config Debug
cmake.exe --build . --target ALL_BUILD --config Release
